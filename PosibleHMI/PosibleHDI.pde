boolean boton1 = false;
boolean boton2 = false;
float x1 = 750;
float y1 = 710;
float x2 = 750;
float y2 = 750;
float x3 = 900;
float y3 = 900;
float x4 = 1920;
float y4 = 900;
float x5 = 1920;
float y5 = 710;
int px,py;
PImage P;
void setup() {
  P=loadImage("Portuariav2.png");
  size(1920, 900);
  background(150, 200, 250); // Color cielo 150, 200, 250
  fill(212, 172, 13);//color tierra
  rect(0,700,900,800);
  fill(255,255,255);
  fill(10,255,0);//color boton de arranque
  circle(1000,100,100);//boton1 de arranque
  fill(255,0,0);//color boton de stop
  circle(1000,210,100);//boton2 de stop
   fill(93, 109, 126);
  rect(750,700,900,10);
  fill(103, 187, 252);  // Color agua
  beginShape();
  vertex(x1, y1);
  vertex(x2, y2);
  vertex(x3, y3);
  vertex(x4, y4);
  vertex(x5,y5);
  endShape(CLOSE);
  px=-70;
  py=68;
}

void draw() {
 image(P,px,py);
 // Botón de arranque
  if ((mouseX > 950) && (mouseX < 1050) && (mouseY > 48) && (mouseY < 148) && mousePressed) {
    boton1 = true;
    fill(255, 255, 0); // Color boton de arranque
    circle(1000, 100, 100); // Boton de arranque
 }
  
  
  // Botón de stop
  if ((mouseX > 950) && (mouseX < 1050) && (mouseY > 160) && (mouseY < 260) && mousePressed) {
    boton2 = true;
    fill(0, 250, 0); // Color boton de stop
    circle(1000, 100, 100); // Boton de stop
    boton1= false;
  
  }
}
